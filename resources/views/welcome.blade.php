@extends('layouts.app')

@section('content')

                <div class="container">
                    <div class="my-3">
                        <div class="swiper-container swiper-container-initialized swiper-container-horizontal">
                             <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                                <div class="swiper-slide swiper-slide-active" style="width: 1200px; margin-right: 30px;">
                                    <a href="https://octaplus.io/web/stores" target="_blank">
                                        <img src="https://file.octaplus.io/public/banner/tF0eqCWDrv356eihAkJw3hBMdMaTNuRVkW4ARxMl.jpeg?1613525231" 
                                        alt="Feb Fabulous Special Upsized Campaign Weekly Highlight" class="m-auto mh-250 w-100">
                                    </a>
                                </div>
                                <div class="swiper-slide swiper-slide-next" style="width: 1200px; margin-right: 30px;">
                                    <a href="https://octaplus.io/web/stores" target="_blank">
                                        <img src="https://file.octaplus.io/public/banner/hqtdDrDRQlkH55eqHzPr3vE8NSdfcu3wteRx9gSt.jpeg?1612921795" 
                                        alt="Feb Fabulous Special Upsized Campaign" class="m-auto mh-250 w-100">
                                    </a>
                                </div>
                                <div class="swiper-slide" style="width: 1200px; margin-right: 30px;">
                                    <a href="https://octaplus.io/web/faq#faq-5" target="_blank">
                                        <img src="https://file.octaplus.io/public/banner/HqM3mpKY2VzFzJ5yf1qyqpZfViRw7KcftYSwxjfm.jpeg?1592899966" 
                                        alt="Convert your cashback into reward points to redeem more attractive prizes!" class="m-auto mh-250 w-100">
                                    </a>
                                </div>
                                <div class="swiper-slide" style="width: 1200px; margin-right: 30px;">
                                    <a href="https://octaplus.io/web/user/wishes/wall#top_nav" target="_blank">
                                        <img src="https://file.octaplus.io/public/banner/qUvVVpTjW74jzduvnLNFz9mByixBHshzlAg3iiOl.jpeg?1606284595" 
                                        alt="Check in daily for greater rewards!!!" class="m-auto mh-250 w-100">
                                    </a>
                                </div> 
                            </div> 
                            <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets">
                                <span class="swiper-pagination-bullet bg-orange" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span>
                            </div>   
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div>
                    </div>
                </div>              
           </div>
           

           <div class="container p-0 mt-4">
               <div class="row">
                   <div class="col-md-6 text-center my-auto">
                       <img src="https://octaplus.io/img/web/search-bg.png" class="d-none d-md-block">
                    </div> 
                    <div class="col-md-6 my-md-auto">
                        <p class="h1 d-none d-md-block"> Find Yours. The best price will talk.</p> 
                        <p class="h4 font-weight-bold d-sm-block d-md-none"> Find Yours. The best price will talk. </p> 
                        <p class="text-muted mb-0"> Your new shopping assistant specializing in searching for the best price across multiple marketplaces! 
                            Psst, you may be able to earn some cashbacks from Octaplus merchants too!</p> 
                            <a href="/web/find-yours/product-price-compare" class="text-orange">
                                Learn More &gt;&gt;
                            </a> 
                            <form class="pt-3">
                                <div class="row mx-0">
                                    <div class="col px-0">
                                        <div id="center_recent_search" class="input-group-append p-0">
                                            <input autocomplete="off" type="text" placeholder="Search for deals, etc" required="required" class="form-control"> 
                                            <!---->
                                        </div> <!---->
                                    </div> 
                                    <div class="col-auto px-0">
                                        <div class="input-group-append p-0">
                                            <button type="submit" class="btn btn-block bg-orange z-index-0 text-white">
                                                <i class="fa fa-search"></i>
                                            </button></div>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="container">
            <div class="row">
                <div class="col-md title-box section-header pb-3">
                    <div class="row align-items-center mx-0">
                        <div class="col text-left mb-2">
                            <span class="title text-capitalize mb-0">
                                    Cashback Stores
                            </span>
                        </div> 
                        <div class="col-auto mb-2">
                            <a href="/web/stores" class="text-orange font-weight-bold">
                                    More Stores 
                            </a>
                        </div> 
                        <div class="col-sm-12">
                            <div class="row border1 rounded-lg mx-0 ">
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" 
                                            src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>Up to 2% Cashback</span></p>
                                    </a>
                                </div>
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" 
                                            src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>4% Upsized</span></p>
                                    </a>
                                </div>
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/motherhood-my/5d5f584869f12b4a0a244cf2/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" 
                                            src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>6% Upsized</span></p>
                                    </a>
                                </div>
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/cotton-on-my/5d8c8d5969f12b2d8e656092/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" 
                                            src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>3% Upsized</span></p>
                                    </a>
                                </div>
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/althea/5e564e1169f12b7966020f32/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" 
                                            src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>5% Upsized</span></p>
                                    </a>
                                </div>
                                <div class="col-6 col-sm-4 text-center p-2">
                                    <a href="https://octaplus.io/store/watsons-my/5cea0a2b69f12b4d8f34e84a/detail">
                                        <div class="d-flex align-items-center height-75 px-2 overflow-hidden">
                                            <img class="img-responsive w-100 mx-auto" 
                                            data-src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" 
                                            src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" 
                                            lazy="loaded"></div> <p class="text-muted text-truncate mt-1 mb-2">
                                                <span>2% Upsized</span></p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md title-box section-header pb-3">
                    <div class="row align-items-center mx-0">
                        <div class="col text-left mb-2">
                            <span class="title text-capitalize mb-0">
                                    Daily Check-In
                            </span>
                        </div> 
                        <div class="col-sm-12">
                            <div class="row border1">
                                <div class="col-auto my-auto">
                                    <img src="https://octaplus.io/img/web/reward/reward-point.png" class="img-responsive w-100">
                                </div> 
                                <div class="col pr-5 my-auto">
                                    <p> Earn up to 300 points while you check-in 7 consecutive days </p>  
                                    <a class="btn bg-orange text-white rounded-lg px-auto px-sm-5"> Check-In Now </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
        <div class="section-header pb-3">
        <div class="row align-items-center">
            <div class="col px-1 custom-border-line-light px-0 text-left">
                <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> 
                    Shopper Review 
                </span>
            </div> 
            <div class="col-auto">
                <a href="https://octaplus.io/web/posts/" class="btn bg-orange text-white rounded-lg px-4">
                    View All 
                </a>
            </div>
        </div>
        <div class="pb-2 has-white-bg shopper_review">
            <div class="swiper-container1 swiper-container-initialized swiper-container-horizontal swiper-container-free-mode"> 
            <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                    <div class="swiper-slide slide-0 swiper-slide-prev" style="width: 290px; margin-right: 30px;">
                        <div class="box-custom-post border rounded-lg hover-highlight">
                            <div class="row mx-0"><div class="col px-0">
                                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                                    <img alt="MCO,CMCO,RMCO Workout tips at home" data-holder-rendered="true" class="post-img" 
                                    data-src="https://file.octaplus.io/public/post/602e293f29d54f25264e3782/41Imt2k1AZ48ZPPDNAJQvmkR2Kfe35XLEWmnOSPj.png" 
                                    src="https://file.octaplus.io/public/post/602e293f29d54f25264e3782/41Imt2k1AZ48ZPPDNAJQvmkR2Kfe35XLEWmnOSPj.png" 
                                    lazy="loaded"></div> <div class="custom-dropdown-post">
                                        <div class="dropdown">
                                            <a id="dropdownBtn_602e293f29d54f25264e3782_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                                            class="btn btn-outline-secondary border-0 rounded-lg px-2">
                                                <em class="fa fa-ellipsis-v">
                                                </em>
                                            </a> 
                                            <div id="dropdown_602e293f29d54f25264e3782_menu" class="dropdown-menu dropdown-menu-post mt-0">
                                                <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                                                <a is_follow="0" class="dropdown-item py-1 w-auto user_5ca471f969f12b24533b8c62">
                                                    <div class="d-flex align-items-center">
                                                        <i class="fas fa-check mr-1"></i> 
                                                        Follow 
				    			                    </div>
                                                </a> <!---->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row my-2 mx-0">
                                <div class="col px-2">
                                    <a href="https://octaplus.io/user/octaplus/5ca471f969f12b24533b8c62/detail" target="_blank" title="Octaplus" class="text-dark">
                                    <div class="d-flex align-items-center">
                                        <img alt="Octaplus" class="post-user-icon rounded-circle mr-1" 
                                        data-src="https://file.octaplus.io/public/profile-image/b42320515e1f96c453d35c3af93769fe.jpg" 
                                        src="https://file.octaplus.io/public/profile-image/b42320515e1f96c453d35c3af93769fe.jpg" 
                                        lazy="loaded"> <span class="font-weight-bold text-truncate">Octaplus</span>
                                    </div>
                                    </a>
                                </div>
                            </div> 
                            <div class="row mx-0"><div class="col">
                                <div class="d-flex align-items-center">
                                    <p class="text-orange mt-2 mb-1 text-capitalize"> article </p> 
                                </div> 
                                <div class="h-45">
                                    <a href="https://octaplus.io/post/mcocmcormco-workout-tips-at-home/602e293f29d54f25264e3782/detail" target="_blank" 
                                    title="MCO,CMCO,RMCO Workout tips at home" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">MCO,CMCO,RMCO Workout tips at home</a>
                                </div> 
                                <div class="h-36">
                                    <a href="https://octaplus.io/post/mcocmcormco-workout-tips-at-home/602e293f29d54f25264e3782/detail" target="_blank" 
                                    title="Stuck at home because of COVID-19? Make exercise part of your at-home routine with awesome home workouts. Try these tips to become a pro at working out at home, 
                                    plus how to stay motivated when no one's watching." class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">
                                    Stuck at home because of COVID-19? Make exercise part of your at-home routine with awesome home workouts. Try these tips to become a pro at working out at home, 
                                    plus how to stay motivated when no one's watching.</a>
                                </div>
                            </div>
                        </div> 
                        <div class="row my-2 mx-0">
                            <div class="col-auto pr-0">
                                <a href="https://octaplus.io/post/mcocmcormco-workout-tips-at-home/602e293f29d54f25264e3782/detail#blogs-helpful" target="_blank" 
                                class="font-size-12 text-black-50">
                                <div class="d-flex align-items-center">
                                    <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                                    <span title="10" class="text-truncate"> 10 </span>
                                </div>
                                </a>
                            </div> 
                            <div class="col pr-0">
                                <div class="d-flex align-items-center">
                                    <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                                    <span title="263" class="font-size-12 text-black-50 text-truncate"> 263 </span>
                                </div>
                            </div> 
                            <div class="col-auto">
                                <a class="btn-link">
                                    <i class="fa fa-bookmark font-18 far text-black-50"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-1 swiper-slide-active" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-post border rounded-lg hover-highlight">
                        <div class="row mx-0">
                            <div class="col px-0">
                                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                                    <img alt="Asus ROG Strix Scar III review" data-holder-rendered="true" class="post-img" 
                                    data-src="https://file.octaplus.io/public/post/601a68c89f984d774c7e6772/B80Sjwn3W2iURdazFLjawTtO0WbW7ztaReyiz9UF.webp" 
                                    src="https://file.octaplus.io/public/post/601a68c89f984d774c7e6772/B80Sjwn3W2iURdazFLjawTtO0WbW7ztaReyiz9UF.webp" lazy="loaded">
                                </div> 
                                <div class="custom-dropdown-post">
                                    <div class="dropdown">
                                        <a id="dropdownBtn_601a68c89f984d774c7e6772_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                                        class="btn btn-outline-secondary border-0 rounded-lg px-2">
                                        <em class="fa fa-ellipsis-v"></em>
                                        </a> 
                                        <div id="dropdown_601a68c89f984d774c7e6772_menu" class="dropdown-menu dropdown-menu-post mt-0">
                                            <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                                            <a is_follow="0" class="dropdown-item py-1 w-auto user_5f4dfd8950ee796b8432f502">
                                                <div class="d-flex align-items-center">
                                                    <i class="fas fa-check mr-1"></i> Follow 
				    			                </div>
                                            </a> <!---->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row my-2 mx-0">
                            <div class="col px-2">
                                <a href="https://octaplus.io/user/wayne-gadget/5f4dfd8950ee796b8432f502/detail" target="_blank" title="Wayne Gadget" 
                                class="text-dark">
                                <div class="d-flex align-items-center">
                                    <img alt="Wayne Gadget" class="post-user-icon rounded-circle mr-1" 
                                    data-src="https://file.octaplus.io/public/profile-image/ffc9213d70ea5106f464224deb69c9a1.jpg" 
                                    src="https://file.octaplus.io/public/profile-image/ffc9213d70ea5106f464224deb69c9a1.jpg" 
                                    lazy="loaded"> <span class="font-weight-bold text-truncate">Wayne Gadget</span>
                                </div>
                                </a>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col">
                                <div class="d-flex align-items-center">
                                    <p class="text-orange mt-2 mb-1 text-capitalize"> story </p> <!---->
                                </div> 
                                <div class="h-45">
                                    <a href="https://octaplus.io/post/asus-rog-strix-scar-iii-review/601a68c89f984d774c7e6772/detail" target="_blank" 
                                    title="Asus ROG Strix Scar III review" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Asus ROG Strix Scar III review</a>
                                </div> 
                                <div class="h-36">
                                    <a href="https://octaplus.io/post/asus-rog-strix-scar-iii-review/601a68c89f984d774c7e6772/detail" target="_blank" 
                                    title="The Strix Scar III's gray hood has a faux-aluminum design brushed so that the lines meet near the center, 
                                    with half of the hood brushed diagonally and the other half rocking a vertical gradient. The lid has an RGB-lit ROG logo, 
                                    and below that there's an off-center cutout revealing the deck. Beyond that, the hinge protrudes outward with a pair of 
                                    copper vents surrounding the additional ports." class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">
                                    The Strix Scar III's gray hood has a faux-aluminum design brushed so that the lines meet near the center, with half of the 
                                    hood brushed diagonally and the other half rocking a vertical gradient. The lid has an RGB-lit ROG logo, and below that there's 
                                    an off-center cutout revealing the deck. Beyond that, the hinge protrudes outward with a pair of copper vents surrounding the additional ports.</a>
                                </div>
                            </div>
                        </div> 
                        <div class="row my-2 mx-0">
                            <div class="col-auto pr-0">
                                <a href="https://octaplus.io/post/asus-rog-strix-scar-iii-review/601a68c89f984d774c7e6772/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
                                <div class="d-flex align-items-center">
                                    <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                                    <span title="766" class="text-truncate"> 766 </span>
                                </div>
                                </a>
                            </div> 
                            <div class="col pr-0">
                                <div class="d-flex align-items-center">
                                    <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                                    <span title="44.9K" class="font-size-12 text-black-50 text-truncate"> 44.9K </span>
                                </div>
                            </div> 
                            <div class="col-auto">
                                <a class="btn-link">
                                    <i class="fa fa-bookmark font-18 far text-black-50"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-2 swiper-slide-next" style="width: 290px; margin-right: 30px;">
                <div class="box-custom-post border rounded-lg hover-highlight">
                    <div class="row mx-0">
                        <div class="col px-0">
                            <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                                <img alt="Asus VivoBook S15 Review" data-holder-rendered="true" class="post-img" 
                                data-src="https://file.octaplus.io/public/post/602222bef7a8c531350eb652/xpOOFYmzp1sMgIyduqJX4WOwx9sG2zdjih7Xkh4B.jpeg" 
                                src="https://file.octaplus.io/public/post/602222bef7a8c531350eb652/xpOOFYmzp1sMgIyduqJX4WOwx9sG2zdjih7Xkh4B.jpeg" 
                                lazy="loaded">
                            </div> 
                            <div class="custom-dropdown-post">
                                <div class="dropdown">
                                    <a id="dropdownBtn_602222bef7a8c531350eb652_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                                    class="btn btn-outline-secondary border-0 rounded-lg px-2">
                                    <em class="fa fa-ellipsis-v"></em>
                                    </a> 
                                    <div id="dropdown_602222bef7a8c531350eb652_menu" class="dropdown-menu dropdown-menu-post mt-0">
                                        <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                                        <a is_follow="0" class="dropdown-item py-1 w-auto user_5ca41c1969f12b0cd4148cb2">
                                            <div class="d-flex align-items-center">
                                                <i class="fas fa-check mr-1"></i> Follow 
				    			    </div>
                                </a> <!---->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-2 mx-0">
                <div class="col px-2">
                    <a href="https://octaplus.io/user/eztec/5ca41c1969f12b0cd4148cb2/detail" target="_blank" title="ezTEC" class="text-dark">
                    <div class="d-flex align-items-center">
                        <img alt="ezTEC" class="post-user-icon rounded-circle mr-1" 
                        data-src="https://file.octaplus.io/public/profile-image/JdtFKB08Ww7W1xxBnMhZbHQxoqCk3AnyMmQOspPt.png" 
                        src="https://file.octaplus.io/public/profile-image/JdtFKB08Ww7W1xxBnMhZbHQxoqCk3AnyMmQOspPt.png" 
                        lazy="loaded"> <span class="font-weight-bold text-truncate">ezTEC</span>
                    </div>
                    </a>
                </div>
            </div> 
            <div class="row mx-0">
                <div class="col">
                    <div class="d-flex align-items-center">
                        <p class="text-orange mt-2 mb-1 text-capitalize"> story </p> <!---->
                    </div> 
                    <div class="h-45"><a href="https://octaplus.io/post/asus-vivobook-s15-review/602222bef7a8c531350eb652/detail" target="_blank" 
                    title="Asus VivoBook S15 Review" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Asus VivoBook S15 Review</a>
                    </div> 
                    <div class="h-36">
                        <a href="https://octaplus.io/post/asus-vivobook-s15-review/602222bef7a8c531350eb652/detail" target="_blank" 
                        title="Asus' VivoBook S15 is a good laptop with a uniquely colorful design and innovative ScreenPad 2.0, but disappointing battery 
                        life and a dull screen hold it back. With a flashy green-and-orange colorway and innovative secondary screen, the VivoBook S15 is 
                        an appealing laptop on the surface. But how does it hold up as a device made for students and young adults? It's a mixed bag." 
                        class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">Asus' VivoBook S15 is a good laptop with a uniquely colorful design and innovative 
                        ScreenPad 2.0, but disappointing battery life and a dull screen hold it back. With a flashy green-and-orange colorway and innovative secondary 
                        screen, the VivoBook S15 is an appealing laptop on the surface. But how does it hold up as a device made for students and young adults? It's a mixed bag.</a>
                    </div>
                </div>
            </div> 
            <div class="row my-2 mx-0">
                <div class="col-auto pr-0">
                    <a href="https://octaplus.io/post/asus-vivobook-s15-review/602222bef7a8c531350eb652/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
                    <div class="d-flex align-items-center">
                        <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                        <span title="611" class="text-truncate"> 611 </span>
                    </div>
                    </a>
                </div> 
                <div class="col pr-0">
                    <div class="d-flex align-items-center">
                        <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                        <span title="35.1K" class="font-size-12 text-black-50 text-truncate"> 35.1K </span>
                    </div>
                </div> 
                <div class="col-auto">
                    <a class="btn-link">
                        <i class="fa fa-bookmark font-18 far text-black-50"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-slide slide-3" style="width: 290px; margin-right: 30px;">
    <div class="box-custom-post border rounded-lg hover-highlight">
        <div class="row mx-0">
            <div class="col px-0">
                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                    <img alt="❤️korea😍" data-holder-rendered="true" class="post-img" 
                    data-src="https://file.octaplus.io/public/post/5fa90b7be8091c21cd65c522/nJbfeSfaFduNcZYVOXSXB7pSZTk7Zo8wME2kl2oS.jpeg" 
                    src="https://file.octaplus.io/public/post/5fa90b7be8091c21cd65c522/nJbfeSfaFduNcZYVOXSXB7pSZTk7Zo8wME2kl2oS.jpeg" lazy="loaded">
                </div> 
                <div class="custom-dropdown-post">
                    <div class="dropdown">
                        <a id="dropdownBtn_5fa90b7be8091c21cd65c522_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                        class="btn btn-outline-secondary border-0 rounded-lg px-2">
                        <em class="fa fa-ellipsis-v"></em></a> 
                        <div id="dropdown_5fa90b7be8091c21cd65c522_menu" class="dropdown-menu dropdown-menu-post mt-0">
                            <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                            <a is_follow="0" class="dropdown-item py-1 w-auto user_5fa7ab83eab5a62531767102">
                                <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i>
                                 Follow 
				    			</div>
                            </a> <!---->
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row my-2 mx-0">
            <div class="col px-2">
                <a href="https://octaplus.io/user/mernay-binti-mojihol/5fa7ab83eab5a62531767102/detail" target="_blank" title="mernay binti mojihol" class="text-dark">
                <div class="d-flex align-items-center">
                    <img alt="mernay binti mojihol" class="post-user-icon rounded-circle mr-1" 
                    data-src="https://octaplus.io/user/5fa7ab83eab5a62531767102/avatar.png" 
                    src="https://octaplus.io/user/5fa7ab83eab5a62531767102/avatar.png" lazy="loaded"> 
                    <span class="font-weight-bold text-truncate">mernay binti mojihol</span>
                </div>
                </a>
            </div>
        </div> 
        <div class="row mx-0">
            <div class="col">
                <div class="d-flex align-items-center">
                    <p class="text-orange mt-2 mb-1 text-capitalize"> story </p> <!---->
                </div> 
                <div class="h-45">
                    <a href="https://octaplus.io/post/korea/5fa90b7be8091c21cd65c522/detail" target="_blank" 
                    title="❤️korea😍" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">❤️korea😍</a>
                </div> 
                <div class="h-36">
                    <a href="https://octaplus.io/post/korea/5fa90b7be8091c21cd65c522/detail" target="_blank" title="Nak cuba-cuba boleh try starter kit. 
                    Make sure pakai sunscreen bila try set yuja 1️⃣ Some By Mi Yuja Niacin Brightening Trial Kit - Brightening- All skin types, kusam, 
                    jeragat &amp; parut gelap.- Main ingredient : Yuja extract, Glutathione, Arbutin, Lotus Flower extract &amp; 10 kinds of vitamins. 
                    #SomeByMi" class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">Nak cuba-cuba boleh try starter kit. Make sure pakai sunscreen bila try set 
                    yuja 1️⃣ Some By Mi Yuja Niacin Brightening Trial Kit - Brightening- All skin types, kusam, jeragat &amp; parut gelap.- Main ingredient : 
                    Yuja extract, Glutathione, Arbutin, Lotus Flower extract &amp; 10 kinds of vitamins. #SomeByMi</a>
                </div>
            </div>
        </div> 
        <div class="row my-2 mx-0">
            <div class="col-auto pr-0">
                <a href="https://octaplus.io/post/korea/5fa90b7be8091c21cd65c522/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
                <div class="d-flex align-items-center">
                    <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                    <span title="840" class="text-truncate"> 840 </span>
                </div>
                </a>
            </div> 
            <div class="col pr-0">
                <div class="d-flex align-items-center">
                    <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                    <span title="45.8K" class="font-size-12 text-black-50 text-truncate"> 45.8K </span>
                </div>
            </div> 
            <div class="col-auto">
                <a class="btn-link">
                    <i class="fa fa-bookmark font-18 far text-black-50"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="swiper-slide slide-4" style="width: 290px; margin-right: 30px;">
<div class="box-custom-post border rounded-lg hover-highlight">
    <div class="row mx-0">
        <div class="col px-0">
            <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                <img alt="Xiaomi Mi 10 Pro review" data-holder-rendered="true" class="post-img" 
                data-src="https://file.octaplus.io/public/post/6022285c5c300e232f3ce1e2/6ekRnDmYBaZkfeaxPFjntQSMlWWA5VxJbnDSO399.jpeg" 
                src="https://file.octaplus.io/public/post/6022285c5c300e232f3ce1e2/6ekRnDmYBaZkfeaxPFjntQSMlWWA5VxJbnDSO399.jpeg" lazy="loaded">
            </div> 
            <div class="custom-dropdown-post">
                <div class="dropdown">
                    <a id="dropdownBtn_6022285c5c300e232f3ce1e2_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    class="btn btn-outline-secondary border-0 rounded-lg px-2">
                    <em class="fa fa-ellipsis-v"></em>
                    </a> 
                    <div id="dropdown_6022285c5c300e232f3ce1e2_menu" class="dropdown-menu dropdown-menu-post mt-0">
                        <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                        <a is_follow="0" class="dropdown-item py-1 w-auto user_5ca41c1969f12b0cd4148cb2">
                            <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i> 
                            Follow 
				    	    </div>
                        </a> <!---->
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col px-2">
            <a href="https://octaplus.io/user/eztec/5ca41c1969f12b0cd4148cb2/detail" target="_blank" title="ezTEC" class="text-dark">
            <div class="d-flex align-items-center">
                <img alt="ezTEC" class="post-user-icon rounded-circle mr-1" 
                data-src="https://file.octaplus.io/public/profile-image/JdtFKB08Ww7W1xxBnMhZbHQxoqCk3AnyMmQOspPt.png" 
                src="https://file.octaplus.io/public/profile-image/JdtFKB08Ww7W1xxBnMhZbHQxoqCk3AnyMmQOspPt.png" lazy="loaded"> 
                <span class="font-weight-bold text-truncate">ezTEC</span>
            </div></a>
        </div>
    </div> 
    <div class="row mx-0">
        <div class="col">
            <div class="d-flex align-items-center">
                <p class="text-orange mt-2 mb-1 text-capitalize"> story </p> <!---->
            </div> 
            <div class="h-45">
                <a href="https://octaplus.io/post/xiaomi-mi-10-pro-review/6022285c5c300e232f3ce1e2/detail" target="_blank" title="Xiaomi Mi 10 Pro review" 
                class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Xiaomi Mi 10 Pro review</a>
            </div> 
            <div class="h-36">
                <a href="https://octaplus.io/post/xiaomi-mi-10-pro-review/6022285c5c300e232f3ce1e2/detail" target="_blank" title="The Xiaomi Mi 10 Pro is an accomplished phone, 
                and its screen, cameras and battery capacity are all what you’d expect from a premium device – in particular, the speakers and charging speeds are some of the best
                 you’ll find in a smartphone these days. However, there are a few too many annoying software quirks for the overall experience to be as smooth as we’d like – and the 
                 price is quite a bit more than you’d expect for a Xiaomi phone." class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">The Xiaomi Mi 10 Pro is an accomplished phone,
                  and its screen, cameras and battery capacity are all what you’d expect from a premium device – in particular, the speakers and charging speeds are some of the best you’ll 
                  find in a smartphone these days. However, there are a few too many annoying software quirks for the overall experience to be as smooth as we’d like – and the price is quite 
                  a bit more than you’d expect for a Xiaomi phone.</a>
                </div>
            </div>
        </div> 
        <div class="row my-2 mx-0">
            <div class="col-auto pr-0">
                <a href="https://octaplus.io/post/xiaomi-mi-10-pro-review/6022285c5c300e232f3ce1e2/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
                <div class="d-flex align-items-center"><img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                <span title="541" class="text-truncate"> 541 </span>
                </div>
                </a>
            </div> 
            <div class="col pr-0">
                <div class="d-flex align-items-center">
                    <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                    <span title="31.3K" class="font-size-12 text-black-50 text-truncate"> 31.3K </span>
                </div>
            </div> 
            <div class="col-auto">
                <a class="btn-link">
                    <i class="fa fa-bookmark font-18 far text-black-50"></i>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="swiper-slide slide-5" style="width: 290px; margin-right: 30px;">
<div class="box-custom-post border rounded-lg hover-highlight">
    <div class="row mx-0">
        <div class="col px-0">
            <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                <img alt="七天让你逛不停雪隆区夜市❤️！" data-holder-rendered="true" class="post-img" 
                data-src="https://file.octaplus.io/public/post/601c3d9b4e64f8363426be32/Ig2wF3WDw6cfS6hwBRFrhj3TsmkBgS7S9FU0rMNm.jpeg" 
                src="https://file.octaplus.io/public/post/601c3d9b4e64f8363426be32/Ig2wF3WDw6cfS6hwBRFrhj3TsmkBgS7S9FU0rMNm.jpeg" lazy="loaded">
            </div> 
            <div class="custom-dropdown-post">
                <div class="dropdown">
                    <a id="dropdownBtn_601c3d9b4e64f8363426be32_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    class="btn btn-outline-secondary border-0 rounded-lg px-2">
                    <em class="fa fa-ellipsis-v"></em></a> 
                    <div id="dropdown_601c3d9b4e64f8363426be32_menu" class="dropdown-menu dropdown-menu-post mt-0">
                        <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                        <a is_follow="0" class="dropdown-item py-1 w-auto user_5c6e4387c650fc3d534f692c">
                            <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i>
                             Follow 
				    		</div>
                        </a> <!---->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-2 mx-0">
        <div class="col px-2">
            <a href="https://octaplus.io/user/hutl/5c6e4387c650fc3d534f692c/detail" target="_blank" title="Hut.L" class="text-dark">
            <div class="d-flex align-items-center"><img alt="Hut.L" class="post-user-icon rounded-circle mr-1" 
            data-src="https://file.octaplus.io/public/profile-image/mGIxvBjCYKWS96fPAEdSdJNtUtvirhGFyD63dY0s.jpeg" 
            src="https://file.octaplus.io/public/profile-image/mGIxvBjCYKWS96fPAEdSdJNtUtvirhGFyD63dY0s.jpeg" lazy="loaded"> 
            <span class="font-weight-bold text-truncate">Hut.L</span>
            </div>
            </a>
        </div>
    </div> 
    <div class="row mx-0">
        <div class="col">
            <div class="d-flex align-items-center">
                <p class="text-orange mt-2 mb-1 text-capitalize"> story </p> <!---->
            </div> 
            <div class="h-45">
                <a href="https://octaplus.io/post/%E4%B8%83%E5%A4%A9%E8%AE%A9%E4%BD%A0%E9%80%9B%E4%B8%8D%E5%81%9C%E9%9B%AA%E9%9A%86%E5%8C%BA%E5%A4%9C%E5%B8%82/601c3d9b4e64f8363426be32/detail" 
                target="_blank" title="七天让你逛不停雪隆区夜市❤️！" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">七天让你逛不停雪隆区夜市❤️！</a>
            </div> 
            <div class="h-36">
                <a href="https://octaplus.io/post/%E4%B8%83%E5%A4%A9%E8%AE%A9%E4%BD%A0%E9%80%9B%E4%B8%8D%E5%81%9C%E9%9B%AA%E9%9A%86%E5%8C%BA%E5%A4%9C%E5%B8%82/601c3d9b4e64f8363426be32/detail" 
                target="_blank" title="星期一：Pasar Malam SS2 PJ区
                                       星期二：Pasar Malam Sri Petaling
                                       星期三：Pasar Malam Taman Connaught
                                       星期四：Plaza Mont Kiara Fiesta Nite Market
                                       星期五：Pasar Malam Taman Segar (Leisure Mall) 
                                       星期六：Pasar Malam Setia Alam
                                       星期天：Pasar Malam Taman Megah" class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">
                                       星期一：Pasar Malam SS2 PJ区
                                       星期二：Pasar Malam Sri Petaling
                                       星期三：Pasar Malam Taman Connaught
                                       星期四：Plaza Mont Kiara Fiesta Nite Market
                                       星期五：Pasar Malam Taman Segar (Leisure Mall) 
                                       星期六：Pasar Malam Setia Alam
                                       星期天：Pasar Malam Taman Megah
                </a>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col-auto pr-0">
            <a href="https://octaplus.io/post/%E4%B8%83%E5%A4%A9%E8%AE%A9%E4%BD%A0%E9%80%9B%E4%B8%8D%E5%81%9C%E9%9B%AA%E9%9A%86%E5%8C%BA%E5%A4%9C%E5%B8%82/601c3d9b4e64f8363426be32/detail#blogs-helpful" 
            target="_blank" class="font-size-12 text-black-50">
            <div class="d-flex align-items-center">
                <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                <span title="761" class="text-truncate"> 761 </span>
            </div>
            </a>
        </div> 
        <div class="col pr-0">
            <div class="d-flex align-items-center">
                <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                <span title="47.1K" class="font-size-12 text-black-50 text-truncate"> 47.1K </span>
            </div>
        </div> 
        <div class="col-auto">
            <a class="btn-link">
                <i class="fa fa-bookmark font-18 far text-black-50"></i>
            </a>
        </div>
    </div>
</div>
</div>
<div class="swiper-slide slide-6" style="width: 290px; margin-right: 30px;">
<div class="box-custom-post border rounded-lg hover-highlight">
    <div class="row mx-0">
        <div class="col px-0">
            <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                <img alt="Simple and Easy" data-holder-rendered="true" class="post-img" 
                data-src="https://file.octaplus.io/public/post/5fc1f2d780ea4e5ab3007e42/dyRHQQZKnIzJNC9o8oJfagL52ZOdORWedjsHPCpw.jpeg" 
                src="https://file.octaplus.io/public/post/5fc1f2d780ea4e5ab3007e42/dyRHQQZKnIzJNC9o8oJfagL52ZOdORWedjsHPCpw.jpeg" lazy="loaded">
            </div> 
            <div class="custom-dropdown-post">
                <div class="dropdown">
                    <a id="dropdownBtn_5fc1f2d780ea4e5ab3007e42_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                    class="btn btn-outline-secondary border-0 rounded-lg px-2">
                    <em class="fa fa-ellipsis-v"></em></a> 
                <div id="dropdown_5fc1f2d780ea4e5ab3007e42_menu" class="dropdown-menu dropdown-menu-post mt-0">
                    <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                    <a is_follow="0" class="dropdown-item py-1 w-auto user_5f32f98726d74321001ce232">
                        <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i> 
                        Follow 
				        </div>
                    </a> <!---->
                </div>
            </div>
        </div>
    </div>
</div> 
<div class="row my-2 mx-0">
    <div class="col px-2">
        <a href="https://octaplus.io/user/muhammad-amir-fahmi/5f32f98726d74321001ce232/detail" target="_blank" title="Muhammad Amir Fahmi" class="text-dark">
        <div class="d-flex align-items-center">
            <img alt="Muhammad Amir Fahmi" class="post-user-icon rounded-circle mr-1" 
            data-src="https://file.octaplus.io/public/profile-image/74f2c8fdbcc79cf0b821a1ea6d5acd1f.jpg" 
            src="https://file.octaplus.io/public/profile-image/74f2c8fdbcc79cf0b821a1ea6d5acd1f.jpg" lazy="loaded"> 
            <span class="font-weight-bold text-truncate">Muhammad Amir Fahmi</span>
        </div>
        </a>
    </div>
</div> 
<div class="row mx-0">
    <div class="col">
        <div class="d-flex align-items-center">
            <p class="text-orange mt-2 mb-1 text-capitalize"> video </p> <!---->
        </div> 
        <div class="h-45">
            <a href="https://octaplus.io/post/simple-and-easy/5fc1f2d780ea4e5ab3007e42/detail" target="_blank" title="Simple and Easy" 
            class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Simple and Easy</a>
        </div> 
        <div class="h-36">
            <a href="https://octaplus.io/post/simple-and-easy/5fc1f2d780ea4e5ab3007e42/detail" target="_blank" title="Simple, easy and beautiful" 
            class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">Simple, easy and beautiful</a>
        </div>
    </div>
</div> 
<div class="row my-2 mx-0">
    <div class="col-auto pr-0">
        <a href="https://octaplus.io/post/simple-and-easy/5fc1f2d780ea4e5ab3007e42/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
        <div class="d-flex align-items-center">
            <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
        <span title="2.2K" class="text-truncate"> 2.2K </span>
        </div>
        </a>
    </div> 
    <div class="col pr-0">
        <div class="d-flex align-items-center">
            <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
            <span title="99.7K" class="font-size-12 text-black-50 text-truncate"> 99.7K </span>
        </div>
    </div> 
    <div class="col-auto">
        <a class="btn-link">
            <i class="fa fa-bookmark font-18 far text-black-50"></i>
        </a>
    </div>
</div>
</div>
</div>
<div class="swiper-slide slide-7" style="width: 290px; margin-right: 30px;">
    <div class="box-custom-post border rounded-lg hover-highlight">
        <div class="row mx-0">
            <div class="col px-0">
                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                    <img alt="Wonderful of Premium Album" data-holder-rendered="true" class="post-img" 
                    data-src="https://file.octaplus.io/public/post/5ff68733cb7dc73e8c74e242/jpi97y0CjJgZCj4QPA30JnOHtZcL2OUJTH3N6Xc9.jpeg" 
                    src="https://file.octaplus.io/public/post/5ff68733cb7dc73e8c74e242/jpi97y0CjJgZCj4QPA30JnOHtZcL2OUJTH3N6Xc9.jpeg" lazy="loaded">
                </div> 
                <div class="custom-dropdown-post">
                    <div class="dropdown">
                        <a id="dropdownBtn_5ff68733cb7dc73e8c74e242_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                        class="btn btn-outline-secondary border-0 rounded-lg px-2">
                        <em class="fa fa-ellipsis-v"></em></a> 
                    <div id="dropdown_5ff68733cb7dc73e8c74e242_menu" class="dropdown-menu dropdown-menu-post mt-0">
                        <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                        <a is_follow="0" class="dropdown-item py-1 w-auto user_5ca41cc269f12b0d1f625fa2">
                            <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i> 
                            Follow 
				    		</div>
                        </a> <!---->
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col px-2">
            <a href="https://octaplus.io/user/photok-malaysia/5ca41cc269f12b0d1f625fa2/detail" target="_blank" title="PhotoK Malaysia" class="text-dark">
            <div class="d-flex align-items-center">
                <img alt="PhotoK Malaysia" class="post-user-icon rounded-circle mr-1" 
                data-src="https://file.octaplus.io/public/profile-image/9myZGONC427LzU3r3OpLABN4CnGEhz1lOInL1k9A.jpeg" 
                src="https://file.octaplus.io/public/profile-image/9myZGONC427LzU3r3OpLABN4CnGEhz1lOInL1k9A.jpeg" lazy="loaded"> 
                <span class="font-weight-bold text-truncate">PhotoK Malaysia</span>
            </div>
            </a>
        </div>
    </div> 
    <div class="row mx-0">
        <div class="col">
            <div class="d-flex align-items-center">
                <p class="text-orange mt-2 mb-1 text-capitalize"> video </p> <!---->
            </div> 
            <div class="h-45">
                <a href="https://octaplus.io/post/wonderful-of-premium-album/5ff68733cb7dc73e8c74e242/detail" target="_blank" title="Wonderful of Premium Album" 
                class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Wonderful of Premium Album</a>
            </div> 
            <div class="h-36">
                <a href="https://octaplus.io/post/wonderful-of-premium-album/5ff68733cb7dc73e8c74e242/detail" target="_blank" title="Wonderful 4 type of Premium 
                Album which is #Rabbit, #Panda, #Dog, #Cat that fit 240pcs lomo card Polaroid Photos and business cards 😎🖼️📸Best prices at RM2x only
                兔兔、狗狗、熊猫、猫咪相册精美登场～🥰️可放240张图片，售价只在RM2x而已。PM for order now while stock last. We provide fast delivery  📦  
                Learn more at IG: photokmalaysia
                Facebook page : www.facebook.com/photokmalaysia/#lomocard #照片 #birthday #album #graduation #photo #valentine #polaroidphoto #anniversary #Love #DIY 
                #情人节 #murah #xmas #christmas #包邮 #present #freeshipping #StringLEDlight #memories #gift #stayathome #fairylight #pospercuma" class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">
                Wonderful 4 type of Premium Album which is #Rabbit, #Panda, #Dog, #Cat that fit 240pcs lomo card Polaroid Photos and business cards 😎🖼️📸Best prices at RM2x only
                兔兔、狗狗、熊猫、猫咪相册精美登场～🥰️可放240张图片，售价只在RM2x而已。PM for order now while stock last. We provide fast delivery  📦  Learn more at
                IG: photokmalaysia Facebook page : www.facebook.com/photokmalaysia/#lomocard #照片 #birthday #album #graduation #photo #valentine #polaroidphoto #anniversary #Love #DIY #情人节 #murah 
                #xmas #christmas #包邮 #present #freeshipping #StringLEDlight #memories #gift #stayathome #fairylight #pospercuma</a>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col-auto pr-0">
            <a href="https://octaplus.io/post/wonderful-of-premium-album/5ff68733cb7dc73e8c74e242/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
            <div class="d-flex align-items-center"><img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
            <span title="2K" class="text-truncate"> 2K </span>
            </div>
            </a>
        </div> 
        <div class="col pr-0">
            <div class="d-flex align-items-center">
                <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                <span title="103.6K" class="font-size-12 text-black-50 text-truncate"> 103.6K </span>
            </div>
        </div> 
        <div class="col-auto">
            <a class="btn-link">
                <i class="fa fa-bookmark font-18 far text-black-50"></i>
            </a>
        </div>
    </div>
</div>
</div>
<div class="swiper-slide slide-8" style="width: 290px; margin-right: 30px;">
    <div class="box-custom-post border rounded-lg hover-highlight">
        <div class="row mx-0">
            <div class="col px-0">
                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                    <img alt="Jom singgal di Video saya" data-holder-rendered="true" class="post-img" 
                    data-src="https://file.octaplus.io/public/post/5fd7198e29d044473c27b502/jecTTPWF1Fd6R9aJGZ5OFppbRAJSl32pwTlsVcBT.png" 
                    src="https://file.octaplus.io/public/post/5fd7198e29d044473c27b502/jecTTPWF1Fd6R9aJGZ5OFppbRAJSl32pwTlsVcBT.png" lazy="loaded">
                </div> 
                <div class="custom-dropdown-post">
                    <div class="dropdown">
                        <a id="dropdownBtn_5fd7198e29d044473c27b502_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                        class="btn btn-outline-secondary border-0 rounded-lg px-2">
                        <em class="fa fa-ellipsis-v"></em></a> 
                    <div id="dropdown_5fd7198e29d044473c27b502_menu" class="dropdown-menu dropdown-menu-post mt-0">
                        <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                        <a is_follow="0" class="dropdown-item py-1 w-auto user_5fd7080278dd39498d03ae12">
                            <div class="d-flex align-items-center"><i class="fas fa-check mr-1"></i> 
                            Follow 
				    		</div>
                        </a> <!---->
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col px-2">
            <a href="https://octaplus.io/user/aisha-brand/5fd7080278dd39498d03ae12/detail" target="_blank" title="AiSha_BRAND" class="text-dark">
            <div class="d-flex align-items-center">
                <img alt="AiSha_BRAND" class="post-user-icon rounded-circle mr-1" 
                data-src="https://octaplus.io/user/5fd7080278dd39498d03ae12/avatar.png" 
                src="https://octaplus.io/user/5fd7080278dd39498d03ae12/avatar.png" lazy="loaded"> 
                <span class="font-weight-bold text-truncate">AiSha_BRAND</span>
            </div>
            </a>
        </div>
    </div> 
    <div class="row mx-0">
        <div class="col">
            <div class="d-flex align-items-center">
                <p class="text-orange mt-2 mb-1 text-capitalize"> video </p> <!---->
            </div> 
            <div class="h-45">
                <a href="https://octaplus.io/post/jom-singgal-di-video-saya/5fd7198e29d044473c27b502/detail" target="_blank" title="Jom singgal di Video saya" 
                class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Jom singgal di Video saya</a>
            </div> 
            <div class="h-36">
                <a href="https://octaplus.io/post/jom-singgal-di-video-saya/5fd7198e29d044473c27b502/detail" target="_blank" title="Tiada yang lebih melainkan ciptaanNYA🤔😘🥰🥰" 
                class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">Tiada yang lebih melainkan ciptaanNYA🤔😘🥰🥰</a>
            </div>
        </div>
    </div> 
    <div class="row my-2 mx-0">
        <div class="col-auto pr-0">
            <a href="https://octaplus.io/post/jom-singgal-di-video-saya/5fd7198e29d044473c27b502/detail#blogs-helpful" target="_blank" class="font-size-12 text-black-50">
            <div class="d-flex align-items-center">
                <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                <span title="2.1K" class="text-truncate"> 2.1K </span>
            </div>
            </a>
        </div> 
        <div class="col pr-0">
            <div class="d-flex align-items-center">
                <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                <span title="99K" class="font-size-12 text-black-50 text-truncate"> 99K </span>
            </div>
        </div> 
        <div class="col-auto">
            <a class="btn-link">
                <i class="fa fa-bookmark font-18 far text-black-50"></i>
            </a>
        </div>
    </div>
</div>
</div>
<div class="swiper-slide slide-9" style="width: 290px; margin-right: 30px;">
    <div class="box-custom-post border rounded-lg hover-highlight">
        <div class="row mx-0">
            <div class="col px-0">
                <div class="post-img-container d-flex align-items-center justify-content-center bg-light cursor-pointer">
                    <img alt="Thean Hou Temple revives Chinese tradition for Year of the Ox" data-holder-rendered="true" class="post-img" 
                    data-src="https://file.octaplus.io/public/post/6020869fa06b756b1d5d24b6/pic-2.jpg" 
                    src="https://file.octaplus.io/public/post/6020869fa06b756b1d5d24b6/pic-2.jpg" lazy="loaded">
                </div> 
                <div class="custom-dropdown-post">
                    <div class="dropdown">
                        <a id="dropdownBtn_6020869fa06b756b1d5d24b6_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                        class="btn btn-outline-secondary border-0 rounded-lg px-2">
                        <em class="fa fa-ellipsis-v"></em></a> 
                        <div id="dropdown_6020869fa06b756b1d5d24b6_menu" class="dropdown-menu dropdown-menu-post mt-0">
                            <i class="fas fa-caret-up text-white custom-arrow-up ml-113"></i> 
                            <a is_follow="0" class="dropdown-item py-1 w-auto user_5e5dc1fd69f12b36d302e372">
                                <div class="d-flex align-items-center">
                                    <i class="fas fa-check mr-1"></i> 
                                    Follow 
				    			</div>
                            </a> <!---->
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row my-2 mx-0">
            <div class="col px-2">
                <a href="https://octaplus.io/user/louis678/5e5dc1fd69f12b36d302e372/detail" target="_blank" title="Louis678" class="text-dark">
                <div class="d-flex align-items-center">
                    <img alt="Louis678" class="post-user-icon rounded-circle mr-1" 
                    data-src="https://octaplus.io/user/5e5dc1fd69f12b36d302e372/avatar.png" 
                    src="https://octaplus.io/user/5e5dc1fd69f12b36d302e372/avatar.png" lazy="loaded"> 
                    <span class="font-weight-bold text-truncate">Louis678</span>
                </div>
                </a>
                </div>
            </div> 
            <div class="row mx-0">
                <div class="col">
                    <div class="d-flex align-items-center">
                        <p class="text-orange mt-2 mb-1 text-capitalize"> article </p> <!---->
                    </div> 
                    <div class="h-45">
                        <a href="https://octaplus.io/post/thean-hou-temple-revives-chinese-tradition-for-year-of-the-ox/6020869fa06b756b1d5d24b6/detail" target="_blank" 
                        title="Thean Hou Temple revives Chinese tradition for Year of the Ox" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Thean Hou Temple revives 
                        Chinese tradition for Year of the Ox</a>
                    </div> 
                    <div class="h-36">
                        <a href="https://octaplus.io/post/thean-hou-temple-revives-chinese-tradition-for-year-of-the-ox/6020869fa06b756b1d5d24b6/detail" target="_blank" 
                        title="Despite the plague of Covid-19, Thean Hou Temple has revived the old and rare Chinese tradition of ‘whipping the spring’." 
                        class="font-size-12 lh-1-2 mb-0 ellipsis-2 text-muted1">Despite the plague of Covid-19, Thean Hou Temple has revived the old and rare Chinese tradition 
                        of ‘whipping the spring’.
                        </a>
                    </div>
                </div>
            </div> 
            <div class="row my-2 mx-0">
                <div class="col-auto pr-0">
                    <a href="https://octaplus.io/post/thean-hou-temple-revives-chinese-tradition-for-year-of-the-ox/6020869fa06b756b1d5d24b6/detail#blogs-helpful" target="_blank" 
                    class="font-size-12 text-black-50">
                    <div class="d-flex align-items-center">
                        <img src="https://octaplus.io/img/web/post/default_help.svg" class="mh-20 mr-1"> 
                        <span title="695" class="text-truncate1"> 695 </span>
                    </div></a>
                </div> 
                <div class="col pr-0">
                    <div class="d-flex align-items-center">
                        <img src="https://octaplus.io/img/web/post/view.svg" class="mr-1"> 
                        <span title="39.8K" class="font-size-12 text-black-50 text-truncate1"> 39.8K </span>
                    </div>
                </div> 
                <div class="col-auto">
                    <a class="btn-link">
                        <i class="fa fa-bookmark font-18 far text-black-50"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>    
<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
</div> 
<div class="swiper-container-horizontal">
    <div slot="pagination" class="swiper-pagination1 position-static mt-3 swiper-pagination-clickable swiper-pagination-bullets">
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 1"></span>
        <span class="swiper-pagination-bullet bg-orange" tabindex="0" role="button" aria-label="Go to slide 2"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 8"></span>
    </div>
</div>
</div>

<div class="container">
        <div class="section-header pb-3">
            <div class="row align-items-center">
             <div class="col px-1 custom-border-line-light px-0 text-left">
                    <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Item You May Like </span>
                </div> 
                <div class="col-auto">
                    <a href="https://octaplus.io/web/deals" class="btn bg-orange text-white rounded-lg px-4">
                        View All 
                    </a>
                </div>
            </div>
        </div>
        <div class="pb-2 has-white-bg item_you_may_like">
            <div class="swiper-container2 swiper-container-initialized swiper-container-horizontal swiper-container-free-mode"> 
                <div class="swiper-wrapper" style="transform: translate3d(0px, 0px, 0px); transition-duration: 0ms;">
                    <div class="swiper-slide slide-0 swiper-slide-active" style="width: 290px; margin-right: 30px;">
                        <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                            <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                                <img data-holder-rendered="true" class="deal-img" 
                                data-src="https://file.octaplus.io/public/deal/d4b2bea5f7bd416d828440197ceb8754.jpeg?1612501748" 
                                src="https://file.octaplus.io/public/deal/d4b2bea5f7bd416d828440197ceb8754.jpeg?1612501748" 
                                lazy="loaded"> <!---->
                            </div> 
                            <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                                <div class="row mx-0">
                                    <div class="col my-auto p-0 border-right">
                                        <a deal_id="601cd0e968200c3ab550b4a2" class="btn-link h5 text-black-50">
                                            <i class="fa fa-heart-o"></i>
                                        </a>
                                    </div> 
                                    <div class="col my-auto p-0">
                                        <a title="Cashback Temporary Not Available" class="btn-cashback text-white rounded-circle bg-secondary border-0">
                                            <i class="fa fa-dollar"></i>
                                        </a>
                                    </div> <!----> <!---->
                                </div>
                            </div> 
                            <div class="row mx-0">
                                <div class="col px-1"><!----> 
                                    <div class="h-45 mt-20">
                                        <a href="https://octaplus.io/deal/malaysia-airlines-special-deal/601cd0e968200c3ab550b4a2/detail" target="_blank" 
                                        title="Malaysia Airlines Special Deal" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Malaysia Airlines Special Deal
                                        </a>
                                    </div> 
                                    <p class="font-size-12 mb-0 text-muted1"> Travel Loka </p> 
                                    <div class="mb-1">
                                        <div class="d-flex align-items-center">
                                            <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                            <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div class="row mx-0">
                                <div class="col px-1 h-45"><!----> <!---->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-1 swiper-slide-next" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                        <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                            <img data-holder-rendered="true" class="deal-img" 
                            data-src="https://file.octaplus.io/public/deal/e6326ffed82afa58892930d64de1cf9e.jpeg?1611512343" src="https://file.octaplus.io/public/deal/e6326ffed82afa58892930d64de1cf9e.jpeg?1611512343" lazy="loaded"> <!---->
                        </div> 
                        <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                            <div class="row mx-0">
                                <div class="col my-auto p-0 border-right">
                                    <a deal_id="600db5ad58f4a3107d3b7162" class="btn-link h5 text-black-50">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                </div> 
                                <div class="col my-auto p-0">
                                    <a title="Cashback Temporary Not Available" class="btn-cashback text-white rounded-circle bg-secondary border-0">
                                        <i class="fa fa-dollar"></i>
                                    </a>
                                </div> <!----> <!---->
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1"><!----> 
                                <div class="h-45 mt-20">
                                    <a href="https://octaplus.io/deal/d41d8cd98f00b204e9800998ecf8427e/600db5ad58f4a3107d3b7162/detail" target="_blank" title="" 
                                    class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark">
                                    </a>
                                </div> 
                                <p class="font-size-12 mb-0 text-muted1"> Travel Loka </p> 
                                <div class="mb-1">
                                    <div class="d-flex align-items-center">
                                        <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                        <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1 h-45"><!----> <!---->
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide slide-2" style="width: 290px; margin-right: 30px;">
                <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                    <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                        <img data-holder-rendered="true" class="deal-img" 
                        data-src="https://file.octaplus.io/public/deal/09ca79f2722132257683fef5c9ccf9d4.jpg?1613679174" 
                        src="https://file.octaplus.io/public/deal/09ca79f2722132257683fef5c9ccf9d4.jpg?1613679174" lazy="loaded"> <!---->
                    </div> 
                    <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                        <div class="row mx-0">
                            <div class="col my-auto p-0 border-right">
                                <a deal_id="602ec7816f13b80509157172" class="btn-link h5 text-black-50">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                            </div> 
                            <div class="col my-auto p-0">
                                <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                                    <i class="fa fa-dollar"></i>
                                </a>
                            </div> <!----> <!---->
                        </div>
                    </div> 
                    <div class="row mx-0">
                        <div class="col px-1"><!----> 
                            <div class="h-45 mt-20">
                                <a href="https://octaplus.io/deal/reversible-78-tight/602ec7816f13b80509157172/detail" target="_blank" title="Reversible 7/8 Tight" 
                                class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Reversible 7/8 Tight
                                </a>
                            </div> 
                            <p class="font-size-12 mb-0 text-muted1"> Cotton On (MY) </p> 
                                <div class="mb-1">
                                    <div class="d-flex align-items-center">
                                        <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                        <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1 h-45">
                                <p title="MYR60.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR60.00  </p> 
                                <p title="MYR62.00 -3%" class="font-size-12 mb-0 text-black-50 text-truncate1">
                                    <del class="mr-1">MYR62.00</del> 
                                    <span> -3% </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-3" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                        <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                            <img data-holder-rendered="true" class="deal-img" 
                            data-src="https://file.octaplus.io/public/deal/e4c37a0e9addfb3250fec944ee637fc6.jpg?1613687774" 
                            src="https://file.octaplus.io/public/deal/e4c37a0e9addfb3250fec944ee637fc6.jpg?1613687774" lazy="loaded"> <!---->
                        </div> 
                        <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                            <div class="row mx-0">
                                <div class="col my-auto p-0 border-right">
                                    <a deal_id="602ed591604e4302110b6132" class="btn-link h5 text-black-50">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                </div> 
                                <div class="col my-auto p-0">
                                    <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                                        <i class="fa fa-dollar"></i>
                                    </a>
                                </div> <!----> <!---->
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1"><!----> 
                                <div class="h-45 mt-20">
                                    <a href="https://octaplus.io/deal/everyday-bonded-longline-bralette-with-cups/602ed591604e4302110b6132/detail" target="_blank" 
                                    title="Everyday Bonded Longline Bralette With Cups" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Everyday Bonded Longline Bralette With Cups
                                    </a>
                                </div> 
                                <p class="font-size-12 mb-0 text-muted1"> Cotton On (MY) </p> 
                                <div class="mb-1">
                                    <div class="d-flex align-items-center">
                                        <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                        <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1 h-45">
                                <p title="MYR35.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR35.00  </p> 
                                <p title="MYR69.00 -49%" class="font-size-12 mb-0 text-black-50 text-truncate1">
                                    <del class="mr-1">MYR69.00</del> 
                                    <span> -49% </span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-4" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                        <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer">
                            <img src="https://octaplus.io/img/icon/hot.svg" class="hot-badge mr-2"> 
                            <img data-holder-rendered="true" class="deal-img" 
                                data-src="https://file.octaplus.io/public/deal/yBB8jwQxHaqmXI0t280U3YoGazZXZs1TGBpN7gyh.png?1559313668" 
                                src="https://file.octaplus.io/public/deal/yBB8jwQxHaqmXI0t280U3YoGazZXZs1TGBpN7gyh.png?1559313668" lazy="loaded"> <!---->
                        </div> 
                        <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                            <div class="row mx-0">
                                <div class="col my-auto p-0 border-right">
                                    <a deal_id="5cf13d0269f12b12637c8762" class="btn-link h5 text-black-50">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                </div> 
                                <div class="col my-auto p-0">
                                    <a title="Cashback Temporary Not Available" class="btn-cashback text-white rounded-circle bg-secondary border-0">
                                        <i class="fa fa-dollar"></i>
                                    </a>
                                </div> <!----> <!---->
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1"><!----> 
                                <div class="h-45 mt-20">
                                    <a href="https://octaplus.io/deal/10-off-with-for-first-time-purchase-with-tripcarteasia/5cf13d0269f12b12637c8762/detail" target="_blank" 
                                    title="10% Off with for first time purchase with Tripcarte.Asia" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">10% Off with for first time purchase with Tripcarte.Asia
                                    </a>
                                </div> 
                                <p class="font-size-12 mb-0 text-muted1"> Tripcarte (MY) </p> 
                                <div class="mb-1">
                                    <div class="d-flex align-items-center">
                                        <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                        <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1 h-45"><!----> <!---->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-5" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                        <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer">
                            <img src="https://octaplus.io/img/icon/hot.svg" class="hot-badge mr-2"> 
                            <img data-holder-rendered="true" class="deal-img" 
                                data-src="https://file.octaplus.io/public/deal/twFV9NRATVfz50hTvpS3VuGc2CFcGRB4boBP4jqc.jpeg?1566181016" 
                                src="https://file.octaplus.io/public/deal/twFV9NRATVfz50hTvpS3VuGc2CFcGRB4boBP4jqc.jpeg?1566181016" lazy="loaded"> <!---->
                        </div> 
                        <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                            <div class="row mx-0">
                                <div class="col my-auto p-0 border-right">
                                    <a deal_id="5d5a068f69f12b75ab04cf52" class="btn-link h5 text-black-50">
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                </div> 
                                <div class="col my-auto p-0">
                                    <a title="Cashback Temporary Not Available" class="btn-cashback text-white rounded-circle bg-secondary border-0">
                                        <i class="fa fa-dollar"></i>
                                    </a>
                                </div> <!----> <!---->
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1"><!----> 
                                <div class="h-45 mt-20">
                                    <a href="https://octaplus.io/deal/purchase-do-not-age-serum-d-n-a-12-enjoy-23-off/5d5a068f69f12b75ab04cf52/detail" target="_blank" 
                                    title="Purchase Do Not Age Serum D. N. A 12 &amp; enjoy 23% OFF" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Purchase Do Not Age Serum D. N. A 12 &amp; enjoy 23% OFF
                                    </a>
                                </div> 
                                <p class="font-size-12 mb-0 text-muted1"> Cellnique (MY) </p> 
                                <div class="mb-1">
                                    <div class="d-flex align-items-center">
                                        <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                        <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="row mx-0">
                            <div class="col px-1 h-45"><!----> <!---->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide slide-6" style="width: 290px; margin-right: 30px;">
                    <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                        <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                        <img data-holder-rendered="true" class="deal-img" 
                        data-src="https://file.octaplus.io/public/deal/16a55d29e927e10ccf759a4eeb97192a.jpg?1613484155" 
                        src="https://file.octaplus.io/public/deal/16a55d29e927e10ccf759a4eeb97192a.jpg?1613484155" lazy="loaded"> <!---->
                        </div> 
                        <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                            <div class="row mx-0"><div class="col my-auto p-0 border-right">
                                <a deal_id="602bcfe99e1c3a068e5f15d2" class="btn-link h5 text-black-50">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                            </div> 
                            <div class="col my-auto p-0">
                                <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                                    <i class="fa fa-dollar"></i>
                                </a>
                            </div> <!----> <!---->
                        </div>
                    </div> 
                    <div class="row mx-0">
                        <div class="col px-1"><!----> 
                            <div class="h-45 mt-20">
                                <a href="https://octaplus.io/deal/tec-santacruztrek-sn13/602bcfe99e1c3a068e5f15d2/detail" target="_blank" title="Tec SantaCruzTrek Sn13" 
                                class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Tec SantaCruzTrek Sn13
                                </a>
                            </div> 
                            <p class="font-size-12 mb-0 text-muted1"> Sports Direct (MY) </p> 
                            <div class="mb-1">
                                <div class="d-flex align-items-center">
                                    <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                    <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="row mx-0">
                        <div class="col px-1 h-45">
                            <p title="MYR199.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR199.00  </p> 
                            <p title=" -" class="font-size-12 mb-0 text-black-50 text-truncate1"><!----> <!----></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide slide-7" style="width: 290px; margin-right: 30px;">
                <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                    <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                    <img data-holder-rendered="true" class="deal-img" 
                    data-src="https://file.octaplus.io/public/deal/d60cc5d1088297d7578288d3323069c4.jpg?1613484155" 
                    src="https://file.octaplus.io/public/deal/d60cc5d1088297d7578288d3323069c4.jpg?1613484155" lazy="loaded"> <!---->
                    </div> 
                    <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                        <div class="row mx-0">
                            <div class="col my-auto p-0 border-right">
                                <a deal_id="602bcfe99e1c3a068e5f15d3" class="btn-link h5 text-black-50">
                                    <i class="fa fa-heart-o"></i>
                                </a>
                            </div> 
                            <div class="col my-auto p-0">
                                <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                                    <i class="fa fa-dollar"></i>
                                </a>
                            </div> <!----> <!---->
                        </div>
                    </div> 
                    <div class="row mx-0">
                        <div class="col px-1"><!----> 
                            <div class="h-45 mt-20">
                                <a href="https://octaplus.io/deal/tec-santacruztrek-sn13/602bcfe99e1c3a068e5f15d3/detail" target="_blank" 
                                title="Tec SantaCruzTrek Sn13" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">Tec SantaCruzTrek Sn13
                                </a>
                            </div> 
                            <p class="font-size-12 mb-0 text-muted1"> Sports Direct (MY) </p> 
                            <div class="mb-1">
                                <div class="d-flex align-items-center">
                                    <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                                    <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mx-0">
                        <div class="col px-1 h-45">
                            <p title="MYR199.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR199.00  </p> 
                            <p title=" -" class="font-size-12 mb-0 text-black-50 text-truncate1"><!----> <!----></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide slide-8" style="width: 290px; margin-right: 30px;">
                <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
                    <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
                    <img data-holder-rendered="true" class="deal-img" 
                    data-src="https://file.octaplus.io/public/deal/3942d7c8bdef6f9bf94117eb6928e2ab.jpg?1613318493" 
                    src="https://file.octaplus.io/public/deal/3942d7c8bdef6f9bf94117eb6928e2ab.jpg?1613318493" lazy="loaded"> <!---->
                </div> 
                <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                    <div class="row mx-0">
                        <div class="col my-auto p-0 border-right">
                            <a deal_id="6029490c70f8590ce14ec5a2" class="btn-link h5 text-black-50">
                                <i class="fa fa-heart-o"></i>
                            </a>
                        </div> 
                        <div class="col my-auto p-0">
                            <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                                <i class="fa fa-dollar"></i>
                            </a>
                        </div> <!----> <!---->
                    </div>
                </div> 
                <div class="row mx-0">
                    <div class="col px-1"><!----> 
                    <div class="h-45 mt-20">
                        <a href="https://octaplus.io/deal/adidas-runfalcon-20-mens-running-shoes/6029490c70f8590ce14ec5a2/detail" target="_blank" 
                        title="adidas Runfalcon 2.0 Mens Running Shoes" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">adidas Runfalcon 2.0 Mens Running Shoes
                        </a>
                    </div> 
                    <p class="font-size-12 mb-0 text-muted1"> Sports Direct (MY) </p> 
                    <div class="mb-1">
                        <div class="d-flex align-items-center">
                            <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                            <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row mx-0">
                <div class="col px-1 h-45">
                    <p title="MYR205.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR205.00  </p> 
                    <p title="MYR246.99 -17%" class="font-size-12 mb-0 text-black-50 text-truncate1">
                        <del class="mr-1">MYR246.99</del> 
                        <span> -17% </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="swiper-slide slide-9" style="width: 290px; margin-right: 30px;">
        <div class="box-custom-deal border rounded-lg hover-highlight pt-2 px-2">
            <div class="px-2 deal-img-container d-flex align-items-center justify-content-center cursor-pointer"><!----> 
            <img data-holder-rendered="true" class="deal-img" 
            data-src="https://file.octaplus.io/public/deal/86ae706045a2eea4b62adfbbaebf6a58.jpg?1613397788" 
            src="https://file.octaplus.io/public/deal/86ae706045a2eea4b62adfbbaebf6a58.jpg?1613397788" lazy="loaded"> <!---->
            </div> 
            <div class="p-1 mx-2 mx-md-3 border text-center bg-white rounded-lg my-2">
                <div class="row mx-0">
                    <div class="col my-auto p-0 border-right">
                        <a deal_id="602a7e6dddc2311ea05e7412" class="btn-link h5 text-black-50">
                            <i class="fa fa-heart-o"></i>
                        </a>
                    </div> 
                    <div class="col my-auto p-0">
                        <a title="Cashback available for this merchant" class="btn-cashback text-white rounded-circle bg-orange border-0">
                            <i class="fa fa-dollar"></i>
                        </a>
                    </div> <!----> <!---->
                </div>
            </div> 
            <div class="row mx-0">
                <div class="col px-1"><!----> 
                    <div class="h-45 mt-20">
                        <a href="https://octaplus.io/deal/v-lite-felix-25l-hiking-backpack-with-rain-cover/602a7e6dddc2311ea05e7412/detail" target="_blank" 
                        title="V-Lite Felix 25L Hiking Backpack with Rain Cover" class="font-14 lh-1-2 mb-0 ellipsis-2 text-dark" style="text-align:left">V-Lite Felix 25L Hiking Backpack with Rain Cover
                        </a>
                    </div> 
                    <p class="font-size-12 mb-0 text-muted1"> Sports Direct (MY) </p> 
                    <div class="mb-1">
                        <div class="d-flex align-items-center">
                            <p class="font-size-12 mb-0 text-grey"> No Rating Info </p> 
                            <p class="font-size-12 ml-1 mb-0 text-grey"> (0 sold) </p>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="row mx-0">
                <div class="col px-1 h-45">
                    <p title="MYR169.00" class="font-14 mb-0 font-weight-bold text-orange text-truncate1"> MYR169.00  </p> 
                    <p title="MYR256.06 -34%" class="font-size-12 mb-0 text-black-50 text-truncate1">
                        <del class="mr-1">MYR256.06</del> 
                        <span> -34% </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>    
<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
</div> 
<div class="swiper-container-horizontal">
    <div slot="pagination" class="swiper-pagination2 position-static mt-3 swiper-pagination-clickable swiper-pagination-bullets">
        <span class="swiper-pagination-bullet bg-orange" tabindex="0" role="button" aria-label="Go to slide 1"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7"></span>
        <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 8"></span>
    </div>
</div>
</div>

    </div>

    <div class="container">
        <div class="section-header pb-3">
            <div class="row align-items-center">
                <div class="col px-1 custom-border-line-light px-0 text-left">
                    <span class="mb-0 title text-capitalize custom-border-title mobile-font-4vw"> Featured Stores </span>
                </div> 
                <div class="col-auto">
                    <a href="https://octaplus.io/web/stores" class="btn bg-orange text-white rounded-lg px-4">
                    View All 
                    </a>
                </div>
            </div>
            <div class="pb-2 has-white-bg featured_stores">
                <div class="swiper-container3 swiper-container-initialized swiper-container-horizontal swiper-container-free-mode"> 
                    <div class="swiper-wrapper" style="transform: translate3d(-1600px, 0px, 0px); transition-duration: 0ms;">
                        <div class="swiper-slide slide-0" style="width: 290px; margin-right: 30px;">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                    <a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail">
                                    <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                    <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                    <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" 
                                    src="https://file.octaplus.io/public/store/0daVOmY4vXTsxBo3neZBBRH3zUSKYGAxVwk9QErj.jpeg" lazy="loaded">
                                    </a>
                                </div> 
                                <p class="text-muted text-truncate mt-4">
                                    <a href="https://octaplus.io/store/zalora-my/5dc0066d69f12b542c071992/detail" title="Zalora (MY)">
                                    <p class="mb-0"> Zalora (MY) </p>
                                    </a>
                                </p> 
                                <p class="text-truncate text-orange">Up to 2% Cashback</p>
                            </div>
                        </div>
                        <div class="swiper-slide slide-1" style="width: 290px; margin-right: 30px;">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                    <a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail">
                                    <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                    <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                    <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" 
                                    src="https://file.octaplus.io/public/store/1q1HnBKVUnptvbNuXspRFdEnpxcQ02GGi4mxpsHC.jpeg" lazy="loaded">
                                    </a>
                                </div> 
                                <p class="text-muted text-truncate my-2 ">
                                    <a href="https://octaplus.io/store/nike-my/5d504ed53aae55288e11cb33/detail" title="Nike (MY)">
                                    <p class="mb-0"> Nike (MY) </p>
                                    </a>
                                </p> 
                                <div class="h-60 px-2">
                                    <p class="mb-2 text-truncate text-orange">4% Upsized</p> 
                                    <p class="font-size-12 text-truncate text-muted mb-0">Up to 3% Cashback</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide slide-2" style="width: 290px; margin-right: 30px;">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                    <a href="https://octaplus.io/store/motherhood-my/5d5f584869f12b4a0a244cf2/detail">
                                    <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                    <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                    <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" 
                                    src="https://file.octaplus.io/public/store/P27CK8g0fKpy7k1Z6D2vh5fUY5OTIrX6UWsL4RN2.jpeg" lazy="loaded">
                                    </a>
                                </div> 
                                <p class="text-muted text-truncate my-2 ">
                                    <a href="https://octaplus.io/store/motherhood-my/5d5f584869f12b4a0a244cf2/detail" title="Motherhood (MY)">
                                    <p class="mb-0"> Motherhood (MY) </p>
                                    </a>
                                </p> 
                                <div class="h-60 px-2">
                                    <p class="mb-2 text-truncate text-orange">6% Upsized</p> 
                                    <p class="font-size-12 text-truncate text-muted mb-0">Up to 4% Cashback</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide slide-3" style="width: 290px; margin-right: 30px;">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                    <a href="https://octaplus.io/store/cotton-on-my/5d8c8d5969f12b2d8e656092/detail">
                                    <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                    <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                    <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" 
                                    src="https://file.octaplus.io/public/store/oupGaOjApNteGM2xPXusEc6KIM35ckP9B9oeZqwU.jpeg" lazy="loaded">
                                    </a>
                                </div> 
                                <p class="text-muted text-truncate my-2 ">
                                    <a href="https://octaplus.io/store/cotton-on-my/5d8c8d5969f12b2d8e656092/detail" title="Cotton On (MY)">
                                    <p class="mb-0"> Cotton On (MY) </p>
                                    </a>
                                </p> 
                                <div class="h-60 px-2">
                                    <p class="mb-2 text-truncate text-orange">3% Upsized</p> 
                                    <p class="font-size-12 text-truncate text-muted mb-0">Up to 2% Cashback</p>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide slide-4 swiper-slide-prev" style="width: 290px; margin-right: 30px;">
                            <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                    <a href="https://octaplus.io/store/althea/5e564e1169f12b7966020f32/detail">
                                    <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                    <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                    <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" 
                                    src="https://file.octaplus.io/public/store/cdFuXoUACsMr6U9u4mJHbBNBGSSUX8SzZroPq1Bc.jpeg" lazy="loaded"></a></div> <p class="text-muted text-truncate my-2 ">
                                        <a href="https://octaplus.io/store/althea/5e564e1169f12b7966020f32/detail" title="Althea">
                                        <p class="mb-0"> Althea </p>
                                        </a>
                                        </p> 
                                        <div class="h-60 px-2">
                                            <p class="mb-2 text-truncate text-orange">5% Upsized</p> 
                                            <p class="font-size-12 text-truncate text-muted mb-0">Up to 3% Cashback</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide slide-5 swiper-slide-active" style="width: 290px; margin-right: 30px;">
                                    <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                        <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                            <a href="https://octaplus.io/store/watsons-my/5cea0a2b69f12b4d8f34e84a/detail">
                                            <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                            <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                            <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" 
                                            src="https://file.octaplus.io/public/store/GrJdW4kMGVecHjSTZUdEfo8uwrb3Rpoc3ZNG0wrF.jpeg" lazy="loaded">
                                            </a>
                                        </div> 
                                        <p class="text-muted text-truncate my-2 ">
                                            <a href="https://octaplus.io/store/watsons-my/5cea0a2b69f12b4d8f34e84a/detail" title="Watsons (MY)">
                                            <p class="mb-0"> Watsons (MY) </p>
                                            </a>
                                        </p> 
                                        <div class="h-60 px-2">
                                            <p class="mb-2 text-truncate text-orange">2% Upsized</p> 
                                            <p class="font-size-12 text-truncate text-muted mb-0">Up to 1% Cashback</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide slide-6 swiper-slide-next" style="width: 290px; margin-right: 30px;">
                                    <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                        <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                            <a href="https://octaplus.io/store/flower-advisor-my/5d5e71fe69f12b15b318b152/detail">
                                            <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                            <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                            <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/HPo3Eu8GJzbbhczoXbUoB7DFxBtAyeKHz8V52Qvk.jpeg" 
                                            src="https://file.octaplus.io/public/store/HPo3Eu8GJzbbhczoXbUoB7DFxBtAyeKHz8V52Qvk.jpeg" lazy="loaded">
                                            </a>
                                        </div> 
                                        <p class="text-muted text-truncate my-2 ">
                                            <a href="https://octaplus.io/store/flower-advisor-my/5d5e71fe69f12b15b318b152/detail" title="Flower Advisor (MY)">
                                            <p class="mb-0"> Flower Advisor (MY) </p>
                                            </a>
                                        </p> 
                                        <div class="h-60 px-2">
                                            <p class="mb-2 text-truncate text-orange">8% Upsized</p> 
                                            <p class="font-size-12 text-truncate text-muted mb-0">Up to 5% Cashback</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide slide-7" style="width: 290px; margin-right: 30px;">
                                    <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                        <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                            <a href="https://octaplus.io/store/hermo-my/5d5e747369f12b16377f9c52/detail">
                                            <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                            <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                            <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/bfU0pTtG55kS740r5YnMVIVeXHOUYA1VjvAag6hJ.jpeg" 
                                            src="https://file.octaplus.io/public/store/bfU0pTtG55kS740r5YnMVIVeXHOUYA1VjvAag6hJ.jpeg" lazy="loaded">
                                            </a>
                                        </div> 
                                        <p class="text-muted text-truncate my-2 ">
                                            <a href="https://octaplus.io/store/hermo-my/5d5e747369f12b16377f9c52/detail" title="Hermo (MY)">
                                            <p class="mb-0"> Hermo (MY) </p>
                                            </a>
                                        </p> 
                                        <div class="h-60 px-2">
                                            <p class="mb-2 text-truncate text-orange">1.5% Upsized</p> 
                                            <p class="font-size-12 text-truncate text-muted mb-0">Up to 1% Cashback</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide slide-8" style="width: 290px; margin-right: 30px;">
                                    <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                        <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                            <a href="https://octaplus.io/store/berrylook/5d5f7e3169f12b5219703212/detail">
                                            <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                            <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                            <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/SCEX8nHjvkTicmEr5dIPfxXFgUSww3sSoyc0CfzQ.jpeg" 
                                            src="https://file.octaplus.io/public/store/SCEX8nHjvkTicmEr5dIPfxXFgUSww3sSoyc0CfzQ.jpeg" lazy="loaded">
                                            </a>
                                        </div> 
                                        <p class="text-muted text-truncate my-2 ">
                                            <a href="https://octaplus.io/store/berrylook/5d5f7e3169f12b5219703212/detail" title="Berrylook">
                                            <p class="mb-0"> Berrylook </p>
                                        </a>
                                        </p> 
                                        <div class="h-60 px-2">
                                            <p class="mb-2 text-truncate text-orange">10% Upsized</p> 
                                            <p class="font-size-12 text-truncate text-muted mb-0">Up to 8% Cashback</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide slide-9" style="width: 290px; margin-right: 30px;">
                                    <div class="border rounded-lg my-2 hover-highlight hover-cursor text-center position-relative">
                                        <div class="box-custom-store d-flex align-items-center justify-content-center px-3">
                                            <a href="https://octaplus.io/store/qoo10-my/5d5f599b69f12b4a43073c92/detail">
                                            <img src="https://octaplus.io/img/icon/popular-store.svg" class="popular-store-badge h-26"> 
                                            <img src="https://octaplus.io/img/icon/feature-store.svg?v2" class="feature-store-badge h-26" style="left: 26px;"> 
                                            <img class="img-responsive w-100 mt-4 pt-2" data-src="https://file.octaplus.io/public/store/aXw7lbfyMNbIPBYk3xVXW0fH1BYTdaKZhYBFWTjv.jpeg" 
                                            src="https://file.octaplus.io/public/store/aXw7lbfyMNbIPBYk3xVXW0fH1BYTdaKZhYBFWTjv.jpeg" lazy="loaded">
                                            </a>
                                        </div> 
                                        <p class="text-muted text-truncate mt-4">
                                            <a href="https://octaplus.io/store/qoo10-my/5d5f599b69f12b4a43073c92/detail" title="Qoo10 (MY)">
                                            <p class="mb-0"> Qoo10 (MY) </p>
                                            </a>
                                        </p> 
                                        <p class="text-truncate text-orange">Up to 1% Cashback</p>
                                    </div>
                                </div>
                            </div>    
                            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                        </div> 
                        <div class="swiper-container-horizontal">
                            <div slot="pagination" class="swiper-pagination3 position-static mt-3 swiper-pagination-clickable swiper-pagination-bullets">
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 1"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 3"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span>
                                <span class="swiper-pagination-bullet bg-orange" tabindex="0" role="button" aria-label="Go to slide 6"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7"></span>
                                <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 8"></span>
                            </div>
                        </div>
                    </div>
        </div>
    

    </div>

<section class="event-featured bg-white mb-3"><div class="card">
    <div class="card-body text-justify">
        <div>
            <p class="h2">
    What is Octaplus
            </p>
            <p>
	Octaplus is a shopping guide app that comes with loads of benefits for the users, you can get cashback for purchases, 
    find some of the best ongoing deals on the web and also take a look at reviews of products and services by our users, 
    and you can also redeem great prizes! 
        </p>

<p>
	Octaplus is also an e-commerce cashback platform for the benefit of retailers and consumers. Creating personalized shopping 
    experiences with exclusive promotions and e-community features for consumers combined with high-level targeting and engagement 
    for brands.We reach for the ultimate goal in retail – a shared experience, a recommendation to act on a like or a dislike. 
    OctaPlus has been developed with both consumer and e–retailer in mind, bringing cash back retail shopping and personalized 
    experiences to the consumer and invaluable, interpretive smart data to the retailer or merchant as well as increased traffic.
</p>

<p>
	Get access to a whole range of exclusive offers. Our mission is to make shopping a better experience. Our vision is to be able 
    to meet all your shopping needs. Be rewarded for being part of the Octaplus family through our Rewards. Share and discover the 
    best ways to shop in our growing community in the Review.
</p>

<p class="h2">
    What Is Cashback?
</p>

<p class="mb-1">
	Earning Cashback as simple as 1-2-3
</p>

<p> 
	Sign up to Octaplus for FREE then browse &amp; activities cash back from your desired store. You will be redirected to the merchant 
    site, just shop and pay like you usually do. Your cashback will automatically be credited to your Octaplus account up to 48 hours and 
    you will be notified via email. The amount paid back (cashback) is a percentage of the amount the customer spent on an item. The 
    cashback percentage varies from retailer to retailer, with some businesses offering higher cashback rates than others.
</p>

<p class="h2">
	How to Get and Withdraw Your Cashback
</p>

<ol>
	<li>
		<p> Log in to your Octaplus account. </p>
	</li>
	<li>
		<p> Look for your favorite online store on Octaplus and click on the store that you’d like to visit. A pop-up will appear and you 
        will be re-directed to the store. Please do not close the tab or your cashback might not be tracked. </p>
	</li>
	<li>
		<p> Shop and complete your order within the same tab so that Octaplus can track your purchase and the amount that you had spent. </p>
	</li>
	<li>
		<p> After you complete a transaction and would like to make a new order, you will need to click through Octaplus again. </p>
	</li>
	<li>
		<p> Repeat this entire process starting at Octaplus every time you make a purchase or you can check out the cashback tips in each 
        store's description at the page bottom. </p>
	</li>
</ol>

<p>
	After you’ve accumulated a substantial amount of cashback in your Octaplus account, you may transfer the sum via Transfer to Bank and then 
    withdraw as cash. Do note that the minimum available balance in your account must be RM10. Your withdrawal request will then be processed 
    and deposited into the selected account between 7 and 14 working days.
</p>

<p class="h2 font-weight-bold">
	Our Popular Stores
</p>

<p class="h3">
	Zalora
</p>

<p>
	ZALORA is the leading name in online fashion shopping, carrying an ever-expanding line of local and international brands tailored for consumers 
    in the region. Our selection of over 50.000 products covers every aspect of fashion, from skirts to suits, sneakers to slip-ons, sportswear to 
    watches, and so much more.Start your style journey by owning a well-rounded range of basics and off-duty essentials. 
</p>

<p>
	ZALORA shoppers can also shop according to the latest trends that are dominating the fashion runway, whether it’s a monochrome edit, athleisure 
    styling, or this season’s highlights.
</p>

<p>
	With the widest selection of fashion apparel, you can possibly find, and outstanding services like lightning-fast shipping, cash-on-delivery, 
    and free returns, ZALORA brings you the best of Malaysia online shopping today!
</p>

<p class="h3">
	Cotton On
</p>

<p>
	Cotton On Group is Australia's largest global retailer, known for its fashion clothing and stationery brands. It has over 1,500 stores in 18 
    countries and employs 22,000 workers globally. It currently operates eight brands; Cotton On, Cotton On Body, Cotton On Kids, Rubi, Typo, 
    Cotton On LOST, Factorie and Supré.
</p>

<p class="h3">
	Nike
</p>

<p>
	NIKE, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and 
    sales of footwear, apparel, equipment, accessories, and services. 
</p>

<p class="h3">
	Charles and Keith
</p>

<p>
	CHARLES &amp; KEITH started with a vision of creating a line of innovative footwear with a clear design aesthetic for the sensible chic women. 
    Prompted by the pursuit to be directional and innovative in the global market, the brand works closely with appointed business partners to 
    develop at a sharp pace.
</p>

<p class="h3">
	Adidas
</p>

<p>
	Adidas designs for and with athletes of all kinds. Creators, who love to change the game. Challenge conventions. Break the rules and define new ones. 
    Then break them again. We supply teams and individuals with athletic clothing pre-match. To stay focussed. We design sports apparel that get you to 
    the finish line. To win the match.
</p>

<p class="h3">
	Sephora
</p>

<p>
	Sephora is a visionary beauty e-retailer in the region, run by a dedicated and experienced affiliate program team. Affiliate marketing has proven to 
    be a key driver, due to Sephora's ever-increasing amount of classic and emerging brands across a broad range of product categories including skincare, 
    color, fragrance, body, smile care, and haircare, in addition to Sephora's own private label.
</p>

<p class="h3">
	Poplook
</p>

<p>
	POPLOOK.com is the leading online shopping destination in Malaysia. Offering you the widest choice in baju kurung, muslimah dresses, maxi Dresses, jubah, 
    tudung, handbags, jewellery. Free delivery for Malaysia and other selected destinations
</p>

<p>
	Modesty, choice, quality, affordability, inclusion and social awareness are the values that underpin the POPLOOK label. With over 1,500 design options; 
    sizes from XS to 4XL; and a seamless online/in-store shopping experience, Malaysia's homegrown Modest Fashion Label hopes to help all women live their 
    best life through fashion. The fashion label carries clothing, headscarves, handbags and shoes as well as a children's range. Each year, the not-for-profit 
    POPLOOK Gives Back campaign channels proceeds to charities that benefit women and children in need. Since its inception in 2009, the label has won numerous 
    accolades, but the most important being the brand of choice to their customers seeking high quality modest fashion.
</p>

<p class="h3">
	Pomelo
</p>

<p>
	Launched in 2013, Pomelo is a modern fashion brand born in Asia with a global mindset: on-trend, online, on-the-go. With an undisputable sense of style at 
    an unparalleled price, Pomelo aims to offer women everywhere their best look to become their best selves. Shipping is fast, free Min. RM 139 and all products 
    are delivered with a 365 day return policy guaranteed.
</p>

<p>
	POMELOFASHION.COM plays host to weekly New Arrivals incorporating basic, core fashion, and trend-led looks.With an ever-growing assortment, Pomelo's style 
    range spans the realms of beachwear to sportswear, and everything in between.From its design studios in Bangkok, to the doorsteps of millions around the world, 
    Pomelo now delivers to over 50 countries globally. Have you tried Pomelo?
</p>

<p class="h3">
	Agoda
</p>

<p>
	Agoda is one of the world’s fastest growing online travel booking platforms. From its beginnings as an e-commerce start-up based in Singapore in 2005, Agoda has 
    grown to offer a global network of 2 million properties in more than 200 countries and territories worldwide.
</p>

<p class="h3">
	Klook
</p>

<p>
	Find discounted attraction tickets, tours with unique experiences, and more! Join local day tours to visit spectacular sights and go on delicious food trips around 
    the city. Upon landing at the airport, we've got all kinds of transfers available for you. Discover and book amazing travel experiences with Klook!
</p>

<p class="h3">
	KKday
</p>

<p>
	KKday provides all kinds of unique experiences. Scuba diving, rock climbing, cooking classes, secret sights, full day tour, tickets, charter service 
    and airport transfers, we’ve got all you want. Arrange your fantastic itinerary for yourself now!
</p>

<p class="h3">
	Photobook
</p>

<p class="mb-1">
	Photobook Malaysia  offers the best personalisable prints such as canvas prints, cards, stationeries, calendars, prints and photo gifts. We believe 
    your memories should be printed on more than just albums that you can share and admire wherever you are - like a tumbler for the on-the-go and a desk 
    calendar for your office table. Don't forget to keep your home looking good with custom wall decor like metal prints and photo tiles, adding a 
    contemporary style at the same time. Make your own gifts for any occasion including wedding, travel, baby milestone, birthday, graduation, special 
    celebration such as Christmas and Valentine's Day. Curate your best photographs and create a custom gift today through a simple online designer that 
    is programmed with useful tools for every personalisation need.
</p>

<p>
	No more worries about finding the perfect gifts because nothing is more unique than personalised gifts from the heart.
</p>

<p class="h3">
	Jeoel Jewellery
</p>

<p class="mb-1">
	JEOEL is more than jewelry. It is not someone's dream to create just another business. It is not about making fast profits. It is about creating an 
    attainable lifestyle.
</p>

<p>
	Joy can be for everyone.
</p>

<p class="mb-1">
	The young lady in her first job.The prudent entrepreneur.The hardworking wife and mother.The blushing young man looking for the perfect expression of love.
</p>

<p>
	JEOEL looks like fine jewelry without the fine jewelry price tag. We believe everyone deserves to enjoy small luxuries, not just the privileged.
</p>

<p class="h3">
	紫藤 Purple Cane
</p>

<p>
	Purple Cane Holdings Sdn.Bhd. (298681 V) established in 1987, with innovative spirit integrated with ambitious and enterprising individuals, aiming at 
    going through a cultural passage. According to Purple Cane founder Mr. Lim Hock Lam, the definition of cultural business establishment: is a human activity 
    of the integration of soul, mind and practices. In a culture scene, it is a sublimation of cultural resources, promoting its productivity from low to high 
    through one or more virtual processes or innovation that consequently and incessantly contribute to systematic and value cultural development in the human 
    community. A cultural business establishment entrepreneur or organization are individual or institution involved in cultural activity as obligation and lifestyle.
</p>

<p class="h3">
	Focus Point Online Store
</p>

<p>
	Focus Point online store offers prescription glasses online at discount prices. Buy quality eyeglasses and contact lenses with us today.
</p>

<p>
	Focus Point is officially recognized by the Malaysia Book of Records as the largest optical retail chain store in Malaysia and also the first and only optical 
    retail chain store to be listed in Bursa Malaysia. With more than 180 outlets nationwide and more than 230 eye care professionals ready to serve you. 
</p>

<p>
	Customers have a wide range of fashionable eyewear to choose from at the concept stores such as Focus Point, Focus Point Signature, Focus Point Concept Store, 
    Focus Point Outlet, Whoosh, Opulence, Eyefont, Solariz and i-Focus.
</p>

<p class="h3">
	Youbeli
</p>

<p>
	Youbeli.com is a premier multi-category online marketplace in Malaysia. Fully owned by Youbuy Online Sdn Bhd, Youbeli provides customers with an incredible 
    shopping experience, providing everything at their fingertips, essentially being a one stop shopping destination.
</p>

<p>
	<span class="h3 font-weight-normal"> Airasia fresh: </span>
	<span> groceries for everyone </span>
</p>

<p>
	Airasia fresh is a new eCommerce platform specialising in Fresh Produce, Frozen Food and Packaged Food. This unique marketplace is powered by Teleport, 
    the logistics arm of airasia. Think of us as your neighbourhood grocery store, with best and freshest produce and food delivered to you the next day! 
    We strive to provide our customers the best value and experience, including secure payment options and the best customer support.
</p>

<p class="h3">
	JD Sport
</p>

<p>
	JD Sports has everything you need to elevate your everyday casual look to eye-catching new heights. Shop the latest footwear from brands like Nike, 
    adidas, Vans and Puma that deliver performance, style and comfort to fit your on-the-go lifestyle. If you’re looking for athletic slides, basketball sneakers, 
    casual shoes, running gear and everything in between, JD Sports has you covered.
</p>

<p class="h3">
	Watsons
</p>

<p>
	One of the most well-known pharmaceutical companies in the country, Watson's is the one-stop destination for all your health and beauty products. 
    Watsons Malaysia has a great selection of products for all your health and beauty needs.
</p>

<p class="h3">
	Sport Directs
</p>

<p>
	Your one stop sport shop for the biggest brands - browse trainers for Men, Women &amp; Kids. Plus sports fashion, clothing &amp; accessories. 
    SportsDirect.com is a British sporting goods retailer, the primary retail asset of Sports Direct International plc. The company was formerly known as Sports Soccer and Sports World, but since 2007 branches of the chain have been re-branded as SportsDirect.com, the domain name of its online presence. 
</p>
</div></div></div></section>

    

</div>
</div>
        <script>
var swiper = new Swiper('.swiper-container3', {
      slidesPerView: 3,
      spaceBetween: 30,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination3',
        clickable: true,
      },
    });
var swiper = new Swiper('.swiper-container2', {
      slidesPerView: 3,
      spaceBetween: 30,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination2',
        clickable: true,
      },
    });

var swiper = new Swiper('.swiper-container1', {
      slidesPerView: 3,
      spaceBetween: 30,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination1',
        clickable: true,
      },
    });
    var swiper = new Swiper('.swiper-container', {
  // Optional parameters
  spaceBetween: 30,
      centeredSlides: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });

    

    
</script>



        @endsection

        