<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css1/main.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css" />
        <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />

        <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
        
        
    
</head>
<body>
    <nav class="navbar navbar-fixed-top"> 
    <div class="container">
        
        <button id="navbar_btn" type="button" class="navbar-toggler pull-left text-dark border-0 bg-tansparent mt-1 collapsed">
            
        </button> 
        <a href="/" class="router-link-exact-active open active">
            <img src="https://octaplus.io/img/logo.png" class="img-responsive mh-30 my-2 d-none d-md-block"> 
            <img src="https://octaplus.io/img/logo.png" class="img-responsive mh-20 my-2 d-md-none d-block"></a> 
            <ul class="nav m-auto py-3">
                <li class="nav-item mr-4 d-none d-md-block">
                    <a href="https://octaplus.io/web/stores" class="nav-link text-dark">
                        <div class="d-flex align-items-center">
                            <img src="https://octaplus.io/img/icon/cashback.svg" title="Cashback" class="svg-invert mr-1">
                             <span class="d-md-none d-lg-inline"> Cashback 
                             </span>
                        </div>
                    </a>
                </li> 
                <li class="nav-item mr-4 d-none d-md-block">
                    <a href="https://octaplus.io/web/deals" class="nav-link text-dark">
                        <div class="d-flex align-items-center">
                            <img src="https://octaplus.io/img/icon/deal.svg" title="Deals" class="svg-invert mr-1"> 
                                <span class="d-md-none d-lg-inline"> Deals 
                                </span>
                        </div>
                    </a>
                </li> 
                <li class="nav-item mr-4 d-none d-md-block">
                    <a href="https://octaplus.io/web/posts/" class="nav-link text-dark">
                        <div class="d-flex align-items-center">
                            <img src="https://octaplus.io/img/icon/review.svg" title="Reviews" class="svg-invert mr-1"> 
                                <span class="d-md-none d-lg-inline"> Reviews 
                                </span>
                        </div>
                    </a>
                </li> 
                <li class="nav-item mr-4 mt-1 d-none d-md-block">
                    <a id="dropdown_more_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link text-dark">
                        <div class="d-flex align-items-center">
                            <i class="fa fa-ellipsis-h"></i>
                        </div>
                    </a> 
                    <div id="dropdown_more_menu_item" aria-labelledby="dropdownMenuButton" class="dropdown-menu left-auto mt-0">
                        
                        <a href="https://octaplus.io/web/product-vouchers/" class="dropdown-item py-2 bg-white text-dark">
                            <img src="https://octaplus.io/img/icon/voucher-black.svg" class="mr-1"> Vouchers
						</a> 
                        <a href="https://octaplus.io/web/rewards/" class="dropdown-item py-2 bg-white text-dark">
                            <img src="https://octaplus.io/img/icon/reward_black.svg" class="mr-1"> Rewards
						</a> 
                        <a href="https://octaplus.io/web/wishes/" class="dropdown-item py-2 bg-white text-dark">
                            <img src="https://octaplus.io/img/icon/wish_black.svg" class="mr-1"> Wish Tree
						</a>
                    <!---->
                    </div>
                </li> <!---->
            </ul> <!----> 
        <ul class="nav float-right">
            <li class="nav-item">
            <ul class="navbar-nav ml-auto">
                <a href="https://octaplus.io/web/faq" class="nav-link text-dark" title="Help">
                <li>
                    <i class="fa fa-question-circle fa-lg"></i>
</a>
                </li>
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-dark" href="{{ route('login') }}">{{ __('Login |') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-dark" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                    
            </li>
        </ul>
    </div>
    
</nav>

        <main class="py-4">
            @yield('content')
            
        </main>

        <footer>
        <div class="main-footer">
            <div class="footer-content pb-0">
                <div class="container"><div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                     <div class="footer-col mb-3">
                        <a href="/web" class="router-link-exact-active open active">
                            <img src="https://octaplus.io/img/logo2.png" class="mh-50 mt-2"></a>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                        <div class="footer-col">
                        <p class="footer-title p-0 m-0"> About Octaplus </p>
                         <ul class="p-1">
                            <li class="list-inline"><a href="/web/about" class="text-dark"> About Us </a></li>
                            <li class="list-inline"><a href="/web/terms" class="text-dark"> Terms of Service </a></li> 
                            <li class="list-inline"><a href="/web/disclaimer" class="text-dark"> Disclaimer </a></li> 
                            <li class="list-inline"><a href="/web/privacy" class="text-dark"> Privacy Policy </a></li> 
                            <li class="list-inline"><a href="/web/cashback/terms" class="text-dark"> Cashback Terms </a></li></ul>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                        <div class="footer-col">
                            <p class="footer-title p-0 m-0"> More From Us </p> 
                                <ul class="p-1">
                                    <li class="list-inline"><a href="/web/faq" class="text-dark"> FAQ </a></li> 
                                    <li class="list-inline"><a href="https://octaplus.io/user/octaplus/5ca471f969f12b24533b8c62/detail" class="text-dark"> Events </a></li> 
                                    <li class="list-inline"><a href="https://blog.octaplus.io" target="_blank" class="text-dark"> Blog </a></li> 
                                    <li class="list-inline"><a href="https://www.facebook.com/pg/octaplus.io/community" target="_blank" class="text-dark"> Customer Reviews </a></li> 
                                    <li class="list-inline"><a href="/web/career" class="text-dark"> Career </a></li></ul>
                        </div>
                    </div> 

                    <div class="col-xl-3 col-lg-3 col-md-4 col-12  ">
                        <div class="row">
                            <div class=" col-xl-12 col-lg-12 col-md-12 col-12  ">
                                <div class="mobile-app-content"><p class="footer-title no-margin pb-2"> Mobile App Download </p> 
                                    <div class="row ">
                                        <div class="col-6  ">
                                            <a target="_blank" href="https://itunes.apple.com/app/octaplus/id1457072183" class="app-icon"><span class="hide-visually">IOS app</span> 
                                            <img src="https://octaplus.io/img/web/site/app_store_badge.svg" alt="Octaplus available on the App Store" title="Octaplus available on the App Store"></a>
                                        </div> 
                                        <div class="col-6  ">
                                            <a target="_blank" href="https://play.google.com/store/apps/details?id=com.alcodes.octaplus" class="app-icon"><span class="hide-visually">Android App</span> 
                                            <img src="https://octaplus.io/img/web/site/google-play-badge.svg" alt="Octaplus available on the Play Store" title="Octaplus available on the Play Store"></a>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            
                            <div class=" col-xl-12 col-lg-12 col-md-12 col-12  ">
                                <div class="mobile-app-content">
                                    <p class="footer-title no-margin pb-2"> Follow Us </p> 
                                        <div>
                                            <div class="sharethis-inline-follow-buttons st-inline-follow-buttons st-#{action_pos}  st-animated" id="st-1">
                                                <div class="st-btn st-first" data-network="facebook" style="display: inline-block;">
                                                    <img alt="facebook sharing button" src="https://platform-cdn.sharethis.com/img/facebook.svg">
                                                </div>
                                                <div class="st-btn" data-network="twitter" style="display: inline-block;">
                                                    <img alt="twitter sharing button" src="https://platform-cdn.sharethis.com/img/twitter.svg">
                                                </div>
                                                <div class="st-btn" data-network="instagram" style="display: inline-block;">
                                                    <img alt="instagram sharing button" src="https://platform-cdn.sharethis.com/img/instagram.svg">
                                                </div>
                                                <div class="st-btn" data-network="youtube" style="display: inline-block;">
                                                    <img alt="youtube sharing button" src="https://platform-cdn.sharethis.com/img/youtube.svg">
                                                </div>
                                                                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            <div class="clearfix.after">
            </div> 
            <div class="col-xl-12 bg-orange p-2">
                <div class="container text-white"> 
                    <div class="copy-info text-center">
                    	© 2019 Octaplus - Buy . Earn . Explore | All Rights Reserved.
                    </div>
                </div>
            </div>
        </div> 
</footer>

</body>
</html>

